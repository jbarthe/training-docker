Symfony API & Elasticsearch training
====================================

Installation
------------

1. Démarrer les containers Docker via  `make init` ou via `docker-compose up -d`
2. Installation des vendors via `make install` ou via `composer install`

Si les 2 dernières commandes fonctionnent vous devriez pouvoir tester si tout
fonctionne via :

* Exécuter `make ssh` et pouvoir lancer `./bin/console` ;
* Ouvrir `http://localhost:8080/` et voir `Welcome to Symfony 3.0.3`.

Plan de formation
-----------------

* Initialisation d'un projet Symfony
    * Bundles et architecture découplée:<br>
      "Create only one bundle called AppBundle for your application logic"<br>
    * (bonus DDD : architecture (Application, Domain, Infrastructure)<br>
      Quand on devient vraiment à l'aise avec Symfony on peut se poser la
      question "pourquoi notre application est un bundle ?".<br>
      Le DDD donne une autre proposition d'organisation de ces sources sous la
      forme :<br>
      `src/Application`, `src/Domain`, `src/Infrastructure`.<br>
      Cet article donne des différentes possibilités d'organisation du projet :
      https://gnugat.github.io/2016/03/16/ultimate-symfony-skeleton.html

* API Rest
    * Quelques standards pour une API en JSON :
        * http://stateless.co/hal_specification.html
        * http://jsonapi.org/
        * http://json-ld.org/
    * Authentification :
        * http://symfony.com/doc/current/cookbook/security/api_key_authentication.html
        * http://jwt.io/introduction/
        * http://oauth.net/2/
    * API Hypermedia (HATEOAS)
        * à compléter
    * Ecosystème PHP / Symfony (libraries, bundles)
        * En soi Symfony embarque le composant
          [Symfony HttpFoundation](http://symfony.com/doc/current/components/http_foundation/index.html)
          qui offre tout ce qui faut pour intéragir avec une requête HTTP.
        * [FOSRestBundle](http://symfony.com/doc/master/bundles/FOSRestBundle/index.html)
          offre beaucoup de fonctionnalités comme la génération du routing, la
          convertion d'un body en JSON vers une requête POST classique, la
          convertion de la réponse dans le bon format, convertion des erreurs
          d'un formulaire, le versionning, etc.<br>
          Donc attention à utiliser en comprenant bien son code et ses fonctionnalités.
        * [JMSSerializerBundle](http://jmsyst.com/bundles/JMSSerializerBundle)
          était pendant longtemps la solution pour exposer des données dans une
          API REST depuis Symfony, mais le bundle et la librarie sont de moins
          en moins maintenus et trop lent et gourmand en mémoire à mon goût :-)
        * Symfony serializer :<br>
              La création de normalizers personnalisés et sa gestion de groupe rendrent
              ce composant aussi puissant que JMS Serializer et surtout beaucoup moins
              gourmand en performance et en mémoire.<br>
              Quelques liens :
                * http://symfony.com/doc/current/components/serializer.html
                * http://thomas.jarrand.fr/blog/serialization/
    * Content negociation
        * https://en.wikipedia.org/wiki/Content_negotiation
        * https://tools.ietf.org/html/rfc7231#section-5.3
        * https://github.com/willdurand/Negotiation
    * CORS
        * https://en.wikipedia.org/wiki/Cross-origin_resource_sharing
        * https://github.com/nelmio/NelmioCorsBundle
    * Documentation d'API :
        * http://swagger.io/
        * https://github.com/nelmio/NelmioApiDocBundle
        * https://github.com/tripit/slate
    * Versioning :
        * http://symfony.com/doc/master/bundles/FOSRestBundle/versioning.html
    * En dehors de l'écosystème Symfony :
        * [Fractal](http://fractal.thephpleague.com/) est une librarie qui permet
          d'exposer des données dans une API REST très facilement et tout en PHP
          au travers d'un intéressant système de Transformer (classes permettant
          de convertir un objet vers un array).

* Outils pratiques autour des API REST :
    * https://github.com/jkbrzt/httpie
    * Extension Postman

* Symfony en vrac
    * Symfony Security :
        * A connaitre, les voters permettent d'étendre facilement les
          autorisations dans Symfony :<br>
          http://symfony.com/doc/current/cookbook/security/voters.html
    * Symfony Form :
        * https://webmozart.io/blog/2015/09/09/value-objects-in-symfony-forms/
        * http://knplabs.com/fr/blog/ddd-day-lyon-domain-driven-design
        * http://symfony.com/doc/current/cookbook/form/data_transformers.html

* Bonnes pratiques
    * PHPUnit + Prophecy
        * https://phpunit.de/manual/current/en/test-doubles.html#test-doubles.prophecy
    * phpSpec
        * http://phpspec.readthedocs.org/en/latest/
    * Behat avec API Rest
        * https://github.com/Behatch/contexts
        * https://github.com/KnpLabs/FriendlyContexts
    * Bonnes pratiques avec Behat (Modelling by example)
        * http://stakeholderwhisperer.com/posts/2014/10/introducing-modelling-by-example
        * https://plus.google.com/109432113206275010693/posts/bSqTyWe3wqV

* Elasticsearch
    * Préparation de lienvironnement de dev (plugins)
        * https://github.com/lmenezes/elasticsearch-kopf
        * https://mobz.github.io/elasticsearch-head/
        * https://www.elastic.co/guide/en/sense/current/installing.html
    * Ecosystème PHP / Symfony (libraries, bundles)
        * https://github.com/ruflin/Elastica
        * https://github.com/FriendsOfSymfony/FOSElasticaBundle
        * https://github.com/elastic/elasticsearch-php
    * Exemple d'aggrégation :
      ```
      {
        "aggregations": {
          "categories": {
            "terms": {
              "field": "category.name"
            },
            "aggregations": {
              "tags": {
                "terms": {
                  "field": "tags.name"
                }
              }
            }
          }
        }
      }
      ```
    * Alternative : [Algolia](https://www.algolia.com/)

* Domain Driven Design (bonus)
    * Value Objects
        * https://jolicode.github.io/value-object-conf/slides/index.html?full#
    * Command pattern & Domain events
        * http://php-and-symfony.matthiasnoback.nl/tags/command%20bus/ (à lire de bas en haut)
    * Repositories
