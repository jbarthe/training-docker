<?php

namespace spec\AppBundle\Service;

use AppBundle\Entity\Message;
use AppBundle\Entity\MessageRepository;
use Doctrine\ORM\EntityManager;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MessageManagerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('AppBundle\Service\MessageManager');
    }

    function let(EntityManager $em)
    {
        $this->beConstructedWith($em);
    }

    function it_should_add_a_message(
        EntityManager $em,
        Message $message
    ) {
        $message
            ->getCreatedAt()
            ->shouldBeCalled()
            ->willReturn(new \DateTime('today'));

        $em->persist($message)->shouldBeCalled();
        $em->flush($message)->shouldBeCalled();

        $this->add($message);
    }

    function it_should_increment_date(
        Message $message
    ) {
        $message->getCreatedAt()->shouldBeCalled()->willReturn(new \DateTime('today'));

        $this
            ->incrementDate($message)
            ->shouldBeTomorrow()
        ;
    }

    public function getMatchers()
    {
        return [
            'beTomorrow' => function ($subject) {
                return $subject == new \DateTime('tomorrow');
            },
        ];
    }
}
