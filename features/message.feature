Feature: As an API consumer I can manage messages

    Background: As an API consumer I use the right Content-Type
        Given I add "Content-Type" header equal to "application/json"
        And I add "Accept" header equal to "application/json"

    Scenario: As an API consumer I need to be authenticated
        Given I send a GET request to "/"
        Then the response status code should be 401

    Scenario: As an API consumer I need to have a valid JWT token
        Given I have an invalid JWT token
        When I send a GET request to "/"
        Then the response status code should be 401

    Scenario: As an API consumer I can list messages
        Given I have a valid JWT token
        When I send a GET request to "/messages"
        Then the response should be in JSON
        And the response status code should be 200
#        And print last JSON response

    Scenario: As an API consumer I can create a message
        Given I have a valid JWT token
        When I send a POST request to "/messages" with body:
            """
            {
                "message": {
                    "text": "Test Message",
                    "category": {
                        "name": "Test Category"
                    },
                    "tags": [
                        {"name": "First tag"},
                        {"name": "Second tag"}
                    ]
                }
            }
            """
        Then the response should be in JSON
        And the response status code should be 201
        And the JSON node "message.text" should be equal to "Test Message"
#        And print last JSON response
