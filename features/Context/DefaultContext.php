<?php

namespace Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Sanpi\Behatch\HttpCall\Request;
use Sanpi\Behatch\Json\Json;

class DefaultContext implements Context, SnippetAcceptingContext
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(Request $request, EntityManager $em)
    {
        $this->request = $request;
        $this->em = $em;
    }

    /**
     * @BeforeScenario
     */
    public function purgeDatabase()
    {
        $purger = new ORMPurger($this->em);
        $purger->purge();
    }

    /**
     * @Given I have a valid JWT token
     */
    public function iHaveAValidJwtToken()
    {
        $this->request->send('POST', '/jwt/token', [
            'username' => 'user',
            'password' => 'password',
        ]);

        $json = new Json($this->request->getContent());

        if (!isset($json->getContent()->token)) {
            throw new \RuntimeException(
                'Invalid JWT token'
            );
        }

        $this->request->setHttpHeader(
            'Authorization',
            sprintf('Bearer %s', $json->getContent()->token)
        );
    }

    /**
     * @Given I have an invalid JWT token
     */
    public function iHaveAnInvalidJwtToken()
    {
        $this->request->setHttpHeader(
            'Authorization',
            'Bearer ImAnInvalidToken'
        );
    }
}
