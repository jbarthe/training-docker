<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Entity\MessageRepository;
use AppBundle\Form\MessageType;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MessageController extends Controller
{
    /**
     * @ApiDoc(
     *     resource = true,
     *     section = "Messages",
     *     description = "List messages",
     *     statusCodes = {
     *         Response::HTTP_OK = "Returned when successful"
     *     }
     * )
     *
     * @View(
     *     statusCode=Response::HTTP_OK
     * )
     * @QueryParam(
     *     name="search",
     *     description="Search a given expression"
     * )
     */
    public function getMessagesAction(ParamFetcher $fetcher)
    {
        $search = $fetcher->get('search');

        // Version Elasticsearch
        $data = $this->get('fos_elastica.index.app.message')->search($search);
        $messages = $data->getResults();

        // Version Elasticsearch convertit en Doctrine entities :
//        $messages = $this->get('fos_elastica.finder.app.message')->find(null);

        // Version Doctrine :
//        $messages = $this->repository()->getAll();

        return [
            'meta' => [
                'count' => count($messages),
            ],
            'messages' => $messages,
        ];
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     section = "Messages",
     *     description = "Displays a message",
     *     statusCodes = {
     *         Response::HTTP_OK = "Returned when successful",
     *         Response::HTTP_NOT_FOUND = "Message not found",
     *     }
     * )
     *
     * @View(
     *     statusCode=Response::HTTP_OK
     * )
     */
    public function getMessageAction($id)
    {
        try {
            $message = $this->repository()->get($id);
        } catch (NoResultException $e) {
            throw new NotFoundHttpException(
                sprintf('Unable to find Message with id "%s"', $id)
            );
        }

        return [
            'message' => $message,
        ];
    }

    /**
     * @ApiDoc(
     *     section = "Messages",
     *     description = "Creates a message",
     *     statusCodes = {
     *         Response::HTTP_CREATED = "Message created",
     *         Response::HTTP_BAD_REQUEST = "Validation errors"
     *     },
     *     input="AppBundle\Form\MessageType"
     * )
     *
     * @View(
     *     statusCode=Response::HTTP_CREATED
     * )
     */
    public function postMessagesAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush($message);

            return [
                'message' => $message,
            ];
        }

        return $form;
    }

    public function putMessagesAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);
    }

    public function patchMessagesAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message, [
            'method' => 'PATCH',
        ]);

        $form->handleRequest($request);
    }

    /**
     * @return MessageRepository
     */
    private function repository()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Message');
    }
}
