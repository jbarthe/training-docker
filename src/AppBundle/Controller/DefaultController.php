<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends Controller
{
    /**
     * @ApiDoc(
     *     resource = true,
     *     description="API homepage",
     *     statusCodes = {
     *         Response::HTTP_OK = "Returned when successful"
     *     }
     * )
     *
     * @Get(path="/")
     * @View(
     *     statusCode=Response::HTTP_OK
     * )
     */
    public function getHomepageAction()
    {
        $messagesUrl = $this->generateUrl(
            'get_messages',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return [
            'messages_url' => $messagesUrl,
        ];
    }
}
