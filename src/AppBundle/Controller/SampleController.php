<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SampleController extends Controller
{
    /**
     * @Route(path="/sample")
     * @Method("POST")
     *
     * @param Request $request
     */
    public function sampleAction(Request $request)
    {
        var_dump($request->getContent());
        var_dump($request->request->all());
        exit;
        //var_dump(json_decode($request->getContent(), true)); exit;
    }
}
