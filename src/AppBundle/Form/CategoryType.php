<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * @var ObjectRepository
     */
    private $repository;

    public function __construct(ObjectRepository $repository)
    {
        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('name', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
        ;

        $builder
            ->get('id')
            ->addModelTransformer(new CallbackTransformer(
                function (Category $category = null) {
                    return null;
                },
                function ($submittedValue) {
                    if (null === $submittedValue) {
                        return null;
                    }

                    $category = $this->repository->find($submittedValue);

                    if (null === $category) {
                        throw new TransformationFailedException(sprintf(
                            'The category with identifier "%s"',
                            $submittedValue
                        ));
                    }

                    return $category;
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Category',
            'empty_data' => function (FormInterface $form) {
                if (null !== $category = $form->get('id')->getData()) {
                    return $category;
                }

                if (null !== $name = $form->get('name')->getData()) {
                    return new Category($name);
                }

                return null;
            }
        ]);
    }
}
