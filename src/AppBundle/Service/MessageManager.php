<?php

namespace AppBundle\Service;

use AppBundle\Entity\Message;
use AppBundle\Entity\MessageRepository;
use Doctrine\ORM\EntityManager;

class MessageManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function add(Message $message)
    {
        $message->getCreatedAt();

        $this->em->persist($message);
        $this->em->flush($message);
    }

    public function incrementDate(Message $message)
    {
        $message->getCreatedAt()->add(new \DateInterval('P1D'));

        return $message->getCreatedAt();
    }
}
