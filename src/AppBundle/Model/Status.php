<?php

namespace AppBundle\Model;

final class Status
{
    const DRAFT = 10;
    const VALIDATED = 100;

    private $value;

    public static function getAvailableStatus()
    {
        return [
            self::DRAFT,
            self::VALIDATED,
        ];
    }

    public static function draft()
    {
        return new self(self::DRAFT);
    }

    public function __construct($value)
    {
        if (!in_array($value, self::getAvailableStatus())) {
            throw new \InvalidArgumentException();
        }

        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function equals($value)
    {
        return $this->value === $value;
    }
}

//class Order {
//    public function __construct()
//    {
//        $this->status = Status::draft();
//    }
//}

//$status1 = Status::draft();
//$status2 = new Status(Status::DRAFT);
//
//$status1 == $status2;
//$status1->equals($status2->getValue());
