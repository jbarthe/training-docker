<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use AppBundle\Entity\Message;
use AppBundle\Entity\Tag;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category = new Category('Actualités');

        $message = new Message();
        $message->setText('En formation chez Norsys');
        $message->setCategory($category);
        $message->addTag(new Tag('norsys'));
        $message->addTag(new Tag('formation'));
        $manager->persist($message);

        $message = new Message();
        $message->setText('Non aux enrhumés');
        $message->setCategory($category);
        $message->addTag(new Tag('norsys'));
        $manager->persist($message);

        $message = new Message();
        $message->setText('L\'équipe');
        $message->setCategory($category);
        $message->addTag(new Tag('norsys'));
        $manager->persist($message);

        $manager->flush();
    }
}
