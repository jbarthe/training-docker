<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Message;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class MessageNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var Message $object */
        return [
            'id' => $object->getId(),
            'text' => $object->getText(),
            'createdAt' => $this->serializer->normalize($object->getCreatedAt(), $format, $context),
            'category' => $this->serializer->normalize($object->getCategory(), $format, $context),
            'tags' => array_map(function ($tag) use ($format, $context) {
                return $this->serializer->normalize($tag, $format, $context);
            }, $object->getTags()->toArray()),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && $data instanceof Message;
    }
}
