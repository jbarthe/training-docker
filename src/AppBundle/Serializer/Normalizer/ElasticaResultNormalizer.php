<?php

namespace AppBundle\Serializer\Normalizer;

use Elastica\Result;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ElasticaResultNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var Result $object */
        return array_merge([
            'id' => $object->getId(),
        ], $object->getData());
    }

    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && $data instanceof Result;
    }
}
