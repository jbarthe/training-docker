<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Tag;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TagNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var Tag $object */
        return [
            'name' => $object->getName(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && $data instanceof Tag;
    }
}
