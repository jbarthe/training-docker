<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Entity\Category;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CategoryNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var Category $object */
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'toto' => 'test',
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && $data instanceof Category;
    }
}
