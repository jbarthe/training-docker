<?php

namespace AppBundle\Serializer\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class DateTimeNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var \DateTime $object */
        return $object->format(DATE_ATOM);
    }

    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && $data instanceof \DateTime;
    }
}
