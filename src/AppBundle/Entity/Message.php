<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * @Entity(repositoryClass="AppBundle\Entity\MessageRepository")
 * @Table(name="message")
 */
class Message
{
    /**
     * @var int
     *
     * @Id()
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @NotBlank()
     *
     * @Column(type="text")
     */
    private $text;

    /**
     * @var Category
     *
     * @NotBlank()
     * @Valid()
     *
     * @ManyToOne(
     *     targetEntity="AppBundle\Entity\Category",
     *     cascade={"persist", "remove"}
     * )
     */
    private $category;

    /**
     * @var ArrayCollection
     *
     * @Valid()
     *
     * @ManyToMany(
     *     targetEntity="AppBundle\Entity\Tag",
     *     cascade={"persist", "remove"}
     * )
     * @JoinTable(
     *     name="message_tag",
     *     joinColumns={
     *         @JoinColumn(name="message_id", referencedColumnName="id", onDelete="CASCADE")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="tag_id", referencedColumnName="id", unique=true, onDelete="CASCADE")
     *     }
     * )
     */
    private $tags;

    /**
     * @var \DateTime
     *
     * @Column(type="date")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->tags = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag)
    {
        $this->tags->add($tag);
    }

    /**
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * @return Tag[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
