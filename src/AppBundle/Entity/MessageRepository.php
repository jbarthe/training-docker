<?php

namespace AppBundle\Entity;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function createQueryWithJoins()
    {
        return $this
            ->createQueryBuilder('m')
            ->select('m, c, t')
            ->leftJoin('m.category', 'c')
            ->leftJoin('m.tags', 't');
    }

    /**
     * @return Message[]
     */
    public function getAll()
    {
        return $this
            ->createQueryWithJoins()
            ->getQuery()
            ->getResult();
    }

    /**
     * @param mixed $id
     *
     * @return Message
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get($id)
    {
        return $this
            ->createQueryWithJoins()
            ->where('m.id = :id')
            ->setParameter('id', (int) $id, Type::INTEGER)
            ->getQuery()
            ->getSingleResult();
    }
}
