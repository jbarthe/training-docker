<?php

namespace AppBundle\Command;

use Elastica\Document;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PureElasticaCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:pure-elastica')
            ->setDescription('Trying to index some data in Elasticsearch')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $elasticaClient = new \Elastica\Client(array(
            'host' => 'elasticsearch',
            'port' => 9200
        ));

        $index = $elasticaClient->getIndex('message_dev');
        $type = $index->getType('test');

        $type->addDocument(new Document(time(), [
            'company' => 'Norsys',
        ]));

        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $message = $em->find('AppBundle:Message', 1);

        $type->addDocument(new Document($message->getId(), [
            'text' => $message->getText(),
            'category' => [
                'name' => $message->getCategory()->getName(),
            ],
        ]));

        $index->refresh();
    }
}
